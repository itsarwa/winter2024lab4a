public class Student{
	
	private String name;
	private int average;
	private String program;
	private int amountLearnt;
	
	//public Student(String n, int avg, String prog, int al){
		//name=n;
		//average=avg;
		//program=prog;
		//amountLearnt=al;
	//}
	
	public Student(String name, int average) {
        this.name = name;
        this.average = average;
        this.program = "Undeclared";
        this.amountLearnt = 0;
    }
	
	public String getName(){
		return this.name;
	}
	
	//public void setName(String newName){
	//	this.name=newName;
	//}
	
	//public int getAverage(){
	//	return this.average;
	//}
	
	public void setAverage(int newAverage){
		this.average=newAverage;
	}
	
	public String getProgram(){
		return this.program;
	}
	
	public void setProgram(String newProgram){
		this.program=newProgram;
	}
	
	public int getAmountLearnt(){
		return this.amountLearnt;
	}
	
	public void setAmountLearnt(int newAmountLearnt){
		this.amountLearnt=newAmountLearnt;
	}
	
	public String sayHi(){
		return "Hi " + name + "!";
	}
	public String yourProgram(){
		return " You're in " + program;
	}
	//public void study(){
	//	amountLearnt++;
	//}
	
	public void study(int amountStudied){
		amountLearnt+=amountStudied;
	}
		
}